#ifndef DELAY_MODULE_HPP
#define DELAY_MODULE_HPP

#include "config.hpp"

SC_MODULE(DelayMod)
{
   //INGRESSI
   sc_in<sc_uint<16> > address_in;
   sc_in<sc_uint<16> > data_in;
   sc_in<sc_uint<1> >  enable_in;
   
   // USCITE
   //sc_out<sc_uint<16> > data_out;
   //sc_out<sc_uint<1> >  enable_out;   
   sc_port<sc_signal_out_if<sc_uint<16> >, 0> data_out;
   sc_port<sc_signal_out_if<sc_uint<1> >, 0> enable_out;
   
   SC_CTOR(DelayMod)
   {	 
     SC_METHOD(delay);
        sensitive << enable_in << data_in << address_in;
        dont_initialize();        
   }
   
   private:
   void delay();
};

#endif
