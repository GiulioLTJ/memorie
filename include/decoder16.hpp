#ifndef DECODER16_HPP
#define DECODER16_HPP

#include "config.hpp"


SC_MODULE(Decoder16)
{
   //INGRESSI
   sc_in<sc_uint<16> > address;  

   // USCITE   
   sc_port<sc_signal_inout_if<sc_uint<1> >,MEM_SIZE> addr_enable;  //Port array
                                    

   SC_CTOR(Decoder16)
   {
     SC_METHOD(decoder);  
        sensitive << address;
   }
   private:
   void decoder(void);
};


#endif
