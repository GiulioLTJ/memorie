//data_memory.hpp
#ifndef DATA_MEMORY_HPP
#define DATA_MEMORY_HPP

#include "config.hpp"
#include "decoder16.hpp"
#include "cell.hpp"
#include "mux16.hpp"
#include "delay_module.hpp"

SC_MODULE(data_memory)
{
   //INGRESSI
   sc_in<sc_uint<16> > address;  
   sc_in<sc_uint<16> > datain;
   sc_in<sc_uint<1> >  enable;
   // USCITE
   sc_out<sc_uint<16> > dataout;
   sc_out<sc_uint<1> >  ready;   
   
   //Canali     
   sc_signal<sc_uint<1> > address_enable[MEM_SIZE];
   sc_signal<sc_uint<1> > enable_s[MEM_SIZE];   
   sc_signal<sc_uint<16> > cell_input[MEM_SIZE];
   sc_signal<sc_uint<16> > cell_output[MEM_SIZE];       
   
   //sottomoduli
   Decoder16 decoder;
   Mux16 mux;
   DelayMod delm;
   sc_vector<Cell> celle;  


   SC_CTOR(data_memory)
      : decoder("dec"), mux("mux"), celle("celle"), delm("delm") 
   {	
	   celle.init(MEM_SIZE);
	   ready.initialize(1);

	   //Binding:
	   decoder.address(this->address);
	   mux.dataout(this->dataout);
	   delm.address_in(this->address);
	   delm.enable_in(this->enable);
	   delm.data_in(this->datain);
	   
	   //per ora provo così:
	   mux.sel(this->address);  
	   
	   for(unsigned i=0; i<MEM_SIZE; i++) {
		   decoder.addr_enable(this->address_enable[i] );
		   celle[i].addr_en(this->address_enable[i]); 
		   delm.data_out(cell_input[i]);
		   celle[i].datain(this->cell_input[i]);		   		   	   
		   delm.enable_out(enable_s[i]);
		   celle[i].enable(this->enable_s[i]); 		      		   	   		   
		   celle[i].dataout(this->cell_output[i]);
		   mux.datain(this->cell_output[i]);
	   }
		   	   	          
       SC_METHOD(memory);       
         sensitive << enable << datain << address;
   }

   private:
   void memory();
};


#endif
