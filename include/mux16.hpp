#ifndef MUX16_HPP
#define MUX16_HPP

#include "config.hpp"

SC_MODULE(Mux16)
{
   //INGRESSI
   sc_in<sc_uint<16> > sel;  
   sc_port<sc_signal_in_if<sc_uint<16> >,MEM_SIZE> datain;  //vettore di porte

   // USCITE   
   sc_out<sc_uint<16> > dataout;
                                    

   SC_CTOR(Mux16)
   {
     SC_METHOD(mux_method);  
        sensitive << sel;
        sensitive << datain;
        
        //~ for(unsigned i=0; i<MEM_SIZE; i++) {
			//~ sensitive << datain[i];
		//~ }
        
   }
   private:
   void mux_method(void);
};


#endif
