#ifndef CELL_HPP
#define CELL_HPP

#include "config.hpp"

SC_MODULE(Cell)
{
   //INGRESSI   
   sc_in<sc_uint<16> > datain;
   
   sc_in<sc_uint<1> > addr_en;  // verrà collegato all'uscita del decoder
   sc_in<sc_uint<1> >  enable;
   
   // USCITE
   sc_out<sc_uint<16> > dataout;
   //sc_out<sc_uint<1> >  ready;  
   
   //DATO
   unsigned data_cell;         
      

   SC_CTOR(Cell)
   {     
     SC_METHOD(memory_cell);
        sensitive << enable << datain << addr_en;
        //dont_initialize();        
   }
   private:
   void memory_cell();
};


#endif
