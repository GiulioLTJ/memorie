#ifndef DATA_MEMORY_COMPORTAMENTALE_HPP
#define DATA_MEMORY_COMPORTAMENTALE_HPP

SC_MODULE(data_memory)
{
   //INGRESSI
   sc_in<sc_uint<16> > address;  // Sono porte!!
   sc_in<sc_uint<16> > datain;
   sc_in<sc_uint<1> >  enable;
   // USCITE
   sc_out<sc_uint<16> > dataout;
   sc_out<sc_uint<1> >  ready;  /* io: è una porta(puntatore a canale),
                                   serve una wait per attualizzarlo */

   SC_CTOR(data_memory)
   {
     SC_THREAD(memory);
        sensitive << enable << datain << address;
   }
   private:
   void memory ();
};


#endif
