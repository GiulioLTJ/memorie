#include <iostream>
#include <iomanip>
#include <systemc.h>
#include <string>
#include "data_memory.hpp"

using namespace std;

int test_failed=0;
sc_event end_of_stimulus;

#define DEBUG 1

SC_MODULE(TestBench)
{      
   static const unsigned TEST_SIZE = 10; //NB: non deve essere maggiore di MEM_SIZE     
   static const unsigned INDIRIZZO_INIZIALE = 3;
   unsigned short in_test[TEST_SIZE];
   unsigned short dato_letto[TEST_SIZE];
	
   sc_signal<sc_uint<16> > address_s;   
   sc_signal<sc_uint<16> > datain_s;  
   sc_signal<sc_uint<1> >  enable_s;
   sc_signal<sc_uint<16> > dataout_s;
   sc_signal<sc_uint<1> >  ready_s;
    
   data_memory mem;  
   
   SC_CTOR(TestBench) : mem("mem")
   {
        SC_THREAD(stimulus_thread);        
        SC_THREAD(checker_thread);                  
        
        //Binding
        mem.address(this->address_s);
        mem.datain(this->datain_s);
        mem.enable(this->enable_s);
        mem.dataout(this->dataout_s);
        mem.ready(this->ready_s);         
        
        init_values();        
   }
  
  
   private:
   void checker_thread()
   {  
	  wait(end_of_stimulus);
	  
	  #if DEBUG
	     //Stampo i vettori di ingresso e di uscita dalla memoria
	     cout << "Stampo i vettori di ingresso e di uscita dalla memoria" << endl;
	     cout << setw(15) << "INGRESSO" << setw(15) << "USCITA" << endl;
	     for(unsigned i=0; i<TEST_SIZE; i++) {
			cout << setw(15) << in_test[i] << setw(15) << dato_letto[i] << endl;
		}
		cout << endl << endl;	 
	  #endif	  
	  
	  //Verifico il vettore dei dati letti
	  for(unsigned j=0; j<TEST_SIZE; j++)
	  {		
         if (dato_letto[j] != in_test[j])
         {
	        cout << "Errore @ " << j << endl;
            cout << "Risultato atteso: " << in_test[j] << endl;
            cout << "Risultato ottenuto :" << dato_letto[j] << endl << endl;
            test_failed++; 
         }
      }
      
      if(test_failed>0)
      {
		 cout << "TEST FALLITO!" << endl;
		 test_failed=1;
	  }
	  else {
	     cout << "-- TEST SUPERATO --" << endl;      
	     test_failed=0;
	  }      
   }
   

   private:   
   void stimulus_thread() 
   {       
      cout << "Stimulus thread started" << endl << endl;
      
         while(ready_s.read()==0)
         {
            wait(1,SC_NS);  
         }      
      
      
      enable_s.write(1);
      /* Lo aggiorno dentro al for che segue, la prima volta che 
      aggiorno indirizzo e datain */
      
      for (unsigned i=0; i<TEST_SIZE; i++) 
      {
        address_s.write(i + INDIRIZZO_INIZIALE);
               
        datain_s.write(in_test[i]);
        
        //Debug testbench
        //cout << "Test, giro num " << i << ", delta cycle num: "<< sc_delta_count();
        //cout << ", scrivo " << in_test[i] << ", " << endl;
                
        wait(SC_ZERO_TIME); //Aggiorno i canali
          
        while(ready_s.read()==0)
        {		
           wait(1,SC_NS);
        }      

        while(ready_s.read()==0)
        {		 
           wait(1,SC_NS);
        }
        
      } 
      //Fine scrittura dati in memoria
      cout<< "Scrittura vettore in memoria finita"<< endl;
      
      enable_s.write(0);  
      /* Lo aggiorno dentro al for che segue, la prima volta che 
      aggiorno l'indirizzo */
            
      cout<<endl;
	  //Leggo la memoria dall'indirizzo iniziale fino a TEST_SIZE
      for (unsigned i=0; i<TEST_SIZE; i++) 
      {
		 cout<<endl;		
	     address_s.write(i+INDIRIZZO_INIZIALE);
	     wait(SC_ZERO_TIME);
	     wait(SC_ZERO_TIME);
	     wait(SC_ZERO_TIME);
	     dato_letto[i]=dataout_s.read();
	  }                
      
      //cout<<"Finito stimulus"<<endl;
      
      
      cout<<"Stampo contenuto memoria\n";
      for(unsigned i=0; i<MEM_SIZE; i++){
		  cout << "mem["<<i<<"] = "<<mem.celle.at(i).data_cell<<endl;
	  }
            
      end_of_stimulus.notify(SC_ZERO_TIME);
   
}

   
   //funzione che genera i valori per il test
   void init_values()
   {
      for (unsigned i=0; i<TEST_SIZE; i++) 
      {
         in_test[i]=1*i + 1;         
      }
   }
   
};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");
  cout << "START TEST" << endl << endl;

  sc_start();  

  return test_failed;
} 
