#include <systemc.h>
#include <string>
#include "instr_mem.hpp"

using namespace std;

SC_MODULE(TestBench)
{
   sc_signal<sc_uint<16> > indirizzo;
   sc_signal<sc_uint<16> > istruzione;

   instr_mem mem;
   
   SC_CTOR(TestBench) : mem("mem")
   {
        SC_THREAD(stimulus_thread);
        mem.indirizzo(this->indirizzo);
        mem.istruzione(this->istruzione);
   }
  
   int check()
   {
      cout << "TEST FINITO" << endl;
      return 0;
   }

   private:

   void stimulus_thread() 
   {
     cout << "START STIMULUS" << endl << endl;
     indirizzo.write(1);
     wait(1,SC_NS);
     indirizzo.write(0);
     wait(1,SC_NS);
     cout << "Indirizzo  : " << indirizzo.read() << endl;
     cout << "Istruzione : " << istruzione.read().to_string(SC_BIN,false) << endl << endl;
     
   }
   
};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START" << endl << endl;

  sc_start();

  return test.check();
} 
