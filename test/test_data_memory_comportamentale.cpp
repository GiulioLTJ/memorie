#include <systemc.h>
#include <string>
#include "data_memory_comportamentale.hpp"

using namespace std;

SC_MODULE(TestBench)
{
   sc_signal<sc_uint<16> > address_s;  //io: sono canali, signal! 
   sc_signal<sc_uint<16> > datain_s;  
   sc_signal<sc_uint<1> >  enable_s;
   sc_signal<sc_uint<16> > dataout_s;
   sc_signal<sc_uint<1> >  ready_s;
    

   data_memory MEM;  //io: istanzio la memporia. Perchè maiuscolo?
   
   SC_CTOR(TestBench) : MEM("MEM")
   {
        SC_THREAD(stimulus_thread);

        MEM.address(address_s);
        MEM.datain(datain_s);
        MEM.enable(enable_s);
        MEM.dataout(dataout_s);
        MEM.ready(ready_s);        
        
        init_values();  //inizializza i valori per il test
   }
  
  
   int check()
   {
      for (unsigned i=0;i<TEST_SIZE;i++) 
      {
        if (dato_letto[i]!=in_test[i])
        {
	    cout << "TEST FALLITO: " << i << endl;
            cout << "RISULTATO TEORICO: " << i+1 << endl;
            cout << "RISULTATO TEST :" << dato_letto[i] << endl;
            return 1; 
        }
      }
      cout << "-- TEST OK --" << endl;
      
      return 0;
   }
   

   private:
   
   void stimulus_thread() 
   {
     cout << "START STIMULUS" << endl << endl;
     for (unsigned i=0;i<TEST_SIZE;i++) 
     {


//io: metti delle printf in sto ciclo!
       while(ready_s.read()==0)
       {
		 cout<<"Dentro al primo while\n"  ;
         wait(1,SC_NS);
       }

       enable_s.write(1);
       address_s.write(i);
       
       //io: scrivo il dato di test nella cella di memoria i-esima
       datain_s.write(in_test[i]);

       
       wait(1,SC_NS);
       //io: stampa solo il primo e l'ultimo dato scritti in memoria
      //io: if (i==0||i==MEM_SIZE-1)
          //cout << "Scritto : " << datain_s.read() << " in " << address_s.read() << endl;
          
       //io:
       cout << "in_test[" << address_s.read() << "] = " << in_test[i] << endl;
                 
          
          
       while(ready_s.read()==0)
       {
		   cout<<"Dentro al secondo while\n"  ;
         wait(1,SC_NS);
       }

       enable_s.write(0);

       wait(1,SC_NS);

       while(ready_s.read()==0)
       {
		   cout<<"Dentro al terzo while\n"  ;
         wait(1,SC_NS);
       }


       wait(SC_ZERO_TIME);
       dato_letto[i]=dataout_s.read(); 
       //io: metto una wait, dato che la lettura non funziona. Non penso sia questo il problema, tuttavia.
       wait(SC_ZERO_TIME);
       
       
       
       //~ if (i==0||i==MEM_SIZE-1)
          //~ cout << "Letto   : " << dataout_s.read() << " da " << address_s.read() << endl << endl;
          
          
       //io:
       //cout << "Letto   : " << dato_letto[i] << " da " << address_s.read() << endl << endl;          
       
       //io:
       cout << "dato_letto[" << address_s.read() << "] = " << dato_letto[i] << endl;
          
     }
   }

   //static const unsigned MEM_SIZE = 1024;
   static const unsigned TEST_SIZE = 40;
   unsigned short in_test[TEST_SIZE];
   unsigned short dato_letto[TEST_SIZE];
   
   //funzione che genera i valori per il test
   void init_values()
   {
     for (unsigned i=0;i<TEST_SIZE;i++) 
     {
       //in_test[i]=4*i;
       in_test[i]=i;
     }
   }
};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START TEST" << endl << endl;

  sc_start();

  return test.check(); 

} 
