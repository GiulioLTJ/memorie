#include <iostream>
#include <systemc.h>
#include <time.h>
#include "decoder16.hpp"

static const unsigned TEST_SIZE = 20;

//se voglio fare dele printf di debug:
#define DEBUG 1



unsigned n=0; //indice per riempire il vettore contenente gli output dei diversi test eseguiti

using namespace std;

SC_MODULE(TestBench) {
    sc_signal<sc_uint<16> > injecter;
    sc_signal<sc_uint<1> > observer[MEM_SIZE];

    Decoder16 dec_i;
    
    SC_CTOR(TestBench) : dec_i("dec_i")
    {
        SC_THREAD(stimulus_thread);
        
        //Binding:
        dec_i.address(injecter);
        for(unsigned i=0; i<MEM_SIZE; i++){
			dec_i.addr_enable(observer[i]);
		}
			        
        init_values(); //Inizializzo il vettore con gli ingressi di prova
    }

    int check() {
        // Stampo i vettori di ingresso e di uscita
        
        #if DEBUG        
           cout << "Vettore di ingresso con i valori test:" << endl;        
           for(unsigned i=0; i<TEST_SIZE; i++){			
			   cout << "input_values[" << i << "] = " << input_values[i] << endl;			
		   }
           cout << endl;
        
           cout << "Vettore di uscita con i valori raccolti:" << endl;        
           for(unsigned i=0; i<TEST_SIZE; i++){			
    			cout << "output_values[" << i << "] = " << output_values[i] << endl;			
	 	   }
           cout << endl;        		
        #endif


        // Controllo correttezza uscite
        for (unsigned i=0;i<TEST_SIZE;i++) {
            if (input_values[i] != output_values[i]) {
				cout << "Test fallito! " << endl;
                return 1;
			}
        }
        
        // Controllo rispetto dei tempi
        if (sc_time_stamp() != sc_time(10*TEST_SIZE,SC_MS)) {
			cout << "Test fallito! " << endl << endl;
            return 1;                   
		}

        cout << "Test superato!" << endl << endl;
        return 0;
    }

  
     void read_output() {
         for(unsigned j=0; j<MEM_SIZE; j++) {			 
			 if(observer[j].read()==1) {				 
				 output_values[n]=j;	
				 //cout << "output_values[" << n << "] = " << output_values[n] << endl;			   
				 //cout<<"valore in uscita: "<< j <<"; n=" << n << endl;
			 }
		 }		 
		 n++;
	 }  
 


  private:
    void stimulus_thread() {

        for (unsigned i=0;i<TEST_SIZE;i++) {
            injecter.write(input_values[i]);    
            wait(10,SC_MS); //passa del tempo e il canale viene attualizzato!
            
            read_output();
        }
    }
    

    sc_uint<16> input_values[TEST_SIZE];
    sc_uint<16> output_values[TEST_SIZE];
    
    void init_values() {
		//Riempio il vettore degli indirizzi di prova con indirizzi casuali
		unsigned casuale;
		srand((unsigned)time(NULL));
		
		for(unsigned t=0; t<TEST_SIZE; t++) {
			casuale=rand() % MEM_SIZE;
			input_values[t]=casuale;
		}			
	}
};

int sc_main(int argc, char** argv) {
	
  TestBench test("test");

  sc_start();

  return test.check();
}
