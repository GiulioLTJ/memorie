#include <iostream>
#include <iomanip>
#include <systemc.h>
#include <time.h>
#include "cell.hpp"

static const unsigned TEST_SIZE = 10;
sc_uint<16> init_val=1;

//se voglio fare delle printf di debug:
#define DEBUG 1



unsigned n; //indice per riempire il vettore contenente gli output dei diversi test eseguiti

using namespace std;

SC_MODULE(TestBench)
{
   sc_signal<sc_uint<1> > address_en_s;  
   sc_signal<sc_uint<16> > datain_s;  
   sc_signal<sc_uint<1> >  enable_s;
   sc_signal<sc_uint<16> > dataout_s;   
    
   Cell cella;
   
   SC_CTOR(TestBench) : cella("cella")
   {
        // Binding
        cella.addr_en(address_en_s);
        cella.datain(datain_s);
        cella.enable(enable_s);
        cella.dataout(dataout_s);                
        
        init_values();  
        	   
        SC_THREAD(stimulus_thread);                        
   }

    int check() {		               
        #if DEBUG 
           cout << "\n\n\nRISULTATI TESTBENCH:" << endl;
           // Stampo i vettori di ingresso e di uscita        
           cout << endl;      
           cout << "Dati in ingresso:   --";        
           cout << "   Address enable:     --";        
           cout << "     Enable:     --";        
           cout << "    Uscita prevista:   --";        
           cout << "Uscita reale:" << endl << endl;
                   
           for(unsigned i=0; i<TEST_SIZE; i++){			
			   cout << "input_data[" << i << "] = " << input_data[i] << "     ";			
			   cout << "addr_enable[" << i << "] = " << input_addr_enable[i] <<  "         ";			
			   cout << "enable[" << i << "] = " << input_enable[i] << "     ";			
			   cout << "expected_output[" << i << "] = " << expected_output_values[i] << "     ";			
			   cout << "output[" << i << "] = " << output_values[i] << endl;
		   }
           cout << endl;        
        #endif        
        
        


        cout << "Istante: " << sc_time_stamp() << endl;
        
        // Controllo correttezza uscite
        for (unsigned i=0;i<TEST_SIZE;i++) {
            if (expected_output_values[i] != output_values[i]) {
				cout << "Test fallito! Vettore di uscita diverso da quello di ingresso" << endl;
                return 1;
			}
        }
        
        // Controllo rispetto dei tempi        
        //Ricordati di tenere conto anche della eventuale latenza della memoria!
        if (sc_time_stamp() != sc_time(10*TEST_SIZE,SC_MS)) {
			cout << "Test fallito! Tempi non rispettati. " << endl << endl;
            return 1;             
		}
        
        
        cout << "Test superato!" << endl << endl;
           
        return 0;
    }



     void read_output() {
		 output_values[n] = dataout_s.read();		 		 
	 }  
 


  private:
    void stimulus_thread() {
		//Inizializzo la cella con un valore noto		
		cout << "Testbench: La cella verrà inizializzata con il valore: " << init_val << endl << endl;				
        datain_s.write(init_val);    
        address_en_s.write(1);    
        enable_s.write(1);                
        wait(SC_ZERO_TIME); 
        
        //Ora leggo il dato, così viene copiato in uscita:
        enable_s.write(0);
        address_en_s.write(1);
        wait(SC_ZERO_TIME);         
        wait(SC_ZERO_TIME); 
        cout << "Valore in uscita: " << dataout_s.read() << endl;
        
        
        //Passo alla cella i vettori test  		
        for (n=0; n<TEST_SIZE; n++) {
            datain_s.write(input_data[n]);    
            address_en_s.write(input_addr_enable[n]);    
            enable_s.write(input_enable[n]); 
            
            cout<<"test:settato un valore test\n";
               
            wait(10,SC_MS); //passa del tempo e i canali vengono attualizzati
   
   
   //new         
        enable_s.write(0);
        address_en_s.write(1);
        wait(SC_ZERO_TIME);                     
        
        wait(SC_ZERO_TIME);  
            
            
            read_output();
        }
    }
    

    sc_uint<16> input_data[TEST_SIZE];
    sc_uint<1>  input_enable[TEST_SIZE];
    sc_uint<1>  input_addr_enable[TEST_SIZE];
    
    sc_uint<16> output_values[TEST_SIZE];
    sc_uint<16> expected_output_values[TEST_SIZE];
    
    void init_values() {
		/* Riempio i vettore dei dati, degli enable e degli
		 * addr_enable con dei valori casuali */
		unsigned casuale;
		srand((unsigned)time(NULL));
		
		for(unsigned t=0; t<TEST_SIZE; t++) {
			casuale=rand() % MEM_SIZE;
			input_data[t]=casuale;
			
			casuale=rand() % 2; //casuale compreso tra 0 e 1
			input_enable[t]=casuale;
			
			casuale=rand() % 2; //casuale compreso tra 0 e 1
			input_addr_enable[t]=casuale;	


			//Creiamo un vettore contente i valori attesi in caso di corretto funzionamento			
			if(input_enable[t]==1 && input_addr_enable[t]==1)
			   expected_output_values[t]=input_data[t];
			else {
				if(t==0)
				   expected_output_values[t]=init_val;
				else   
				   expected_output_values[t]=expected_output_values[t-1];
			   }			   			   
		}	
	}
};

int sc_main(int argc, char** argv) {

  TestBench test("test");

  sc_start();
  
  return test.check();
}
