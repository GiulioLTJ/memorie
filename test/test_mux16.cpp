#include <iostream>
#include <systemc.h>
#include <time.h>
#include "mux16.hpp"
#include <iomanip>

static const unsigned TEST_SIZE = 100;


#define DEBUG 1


unsigned n; //indice per riempire il vettore contenente gli output dei diversi test eseguiti

using namespace std;

SC_MODULE(TestBench)
{
   sc_signal<sc_uint<16> > sel_inj;
   sc_signal<sc_uint<16> > input_inj[MEM_SIZE];
   sc_signal<sc_uint<16> > output_obs;
    
   Mux16 mux_i;
   
   SC_CTOR(TestBench) : mux_i("mux")
   {	      	    
       SC_THREAD(stimulus_thread);
        
       //Binding:
       mux_i.sel(sel_inj);
       mux_i.dataout(output_obs);
       for(unsigned i=0; i<MEM_SIZE; i++) {
	      mux_i.datain(input_inj[i]);
	   }
			        
       init_values(); //Inizializzo il vettore con gli ingressi di prova
    }

    int check() {		
        cout << "Istante: " << sc_time_stamp() << endl;
        
        // Controllo correttezza uscite
        for (unsigned i=0;i<TEST_SIZE;i++) {
            if (expected_output_values[i] != output_values[i]) {
				cout << "Test fallito! Vettore di uscita diverso da quello di ingresso" << endl;
                return 1;
			}
        }
        
        // Controllo rispetto dei tempi        
        //Ricordati di tenere conto anche della latenza della memoria!
        if (sc_time_stamp() != sc_time(10*TEST_SIZE,SC_MS)) {
			cout << "Test fallito! Tempi non rispettati. " << endl << endl;
            return 1;             
		}
        
        
        cout << "Test superato!" << endl << endl;
           
        return 0;
    }



     void read_output() {
		 output_values[n] = output_obs.read();		 		 
	 }  
 


  private:
    void stimulus_thread() {		
		unsigned selector = 0;
		unsigned dato = 0;
		unsigned dado;		
				
		//Passo il vettore iniziale e il selettore al mux:
		for(unsigned g=0; g<MEM_SIZE; g++) {
			input_inj[g].write(input_data[g]);
		}				
		sel_inj.write(selector);
		
		wait(SC_ZERO_TIME); //aggiorno	
		
		 				
		       
		srand((unsigned)time(NULL));
		
		//Inizia il test:
        for (n=0; n<TEST_SIZE; n++) {
			//lancio il dado:
			dado=rand() % 2; //casuale compreso tra 0 e 1
			
			if(dado==0) { //cambio il valore del selettore				
				selector=rand() % MEM_SIZE;
				//passo al mux il nuovo valore del selettore
				sel_inj.write(selector);
			}
			else { //cambio il valore del dato per l'ingresso già selezionato
				dato=rand() % MEM_SIZE;				
				input_inj[selector].write(dato);				
			}
			
			wait(10,SC_MS); //aggiorno ingressi o selettore	
			
			expected_output_values[n] = input_inj[sel_inj.read()].read();
            
            read_output();
			
			
		    #if DEBUG
		      //NB: da usarsi solo con una MEM_SIZE piccola, affinchè sia leggibile		  
		      cout << setw(6) << "Input" << setw(20) << "Selettore" << setw(20) << "Output mux" << endl;
		      cout << setw(6) << input_inj[0] << setw(20) << selector << setw(20) << output_obs.read() << endl;
		      for(unsigned z=1; z<MEM_SIZE; z++) {
 			      cout << setw(6) << input_inj[z] << endl;
		      }
		      cout << endl << endl;		
		    #endif			
        }
    }
    

    sc_uint<16> input_data[MEM_SIZE];    
    
    sc_uint<16> output_values[TEST_SIZE];
    sc_uint<16> expected_output_values[TEST_SIZE];
    
    void init_values() {
		/* Riempio il vettore degli input. Verrà usato per inizializzare il mux */
		unsigned casuale;
		srand((unsigned)time(NULL));
		
		for(unsigned t=0; t<MEM_SIZE; t++) {
			casuale=rand() % MEM_SIZE;  //Scelta arbitraria
			input_data[t]=casuale;
		}	
	}
};

int sc_main(int argc, char** argv) {

  TestBench test("test");

  sc_start();
  
  return test.check();
}
