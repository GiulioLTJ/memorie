#include <systemc.h>
#include "delay_module.hpp"

#define DEBUG_SYSTEMC


using namespace std;

void DelayMod :: delay()
{ 
	for(unsigned i=0; i<MEM_SIZE; i++)  
	{
		enable_out[i]->write(enable_in.read());
		data_out[i]->write(data_in.read());	
	}
	
	//cout<<"Path-delay, delta cycle number: "<<sc_delta_count()<<endl;

	return;
}



