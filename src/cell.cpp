#include <systemc.h>
#include "cell.hpp"

#define DEBUG 0

using namespace std;

void Cell::memory_cell()
{  
   //data_cell = 0;  //Inizializzo singola cella di memoria      
      
   #if DEBUG
      //cout << "Memory cell: Initial status: cell = " << data_cell << endl;         
   #endif 

   if (enable->read()==1 && addr_en->read()==1) //scrivi dato 
   {        
      data_cell=datain->read();         

      #if DEBUG
         cout << "-----------------------------"<<endl;
         //cout << "Cell, delta cycle num: " << sc_delta_count()<<endl;
         cout << "Cell, writing: cell <= " << data_cell << endl;         
         cout << "-----------------------------"<<endl;
      #endif
         
      //insert delay scrittura

   }
      
   else
   {
      if(enable->read()==0 && addr_en->read()==1) //leggi dato
      {
	     //~ dataout->write(data_cell);
		              
         #if DEBUG
            cout << "-----------------------------"<<endl;
            //cout << "Cell, delta cycle num: " << sc_delta_count()<<endl;
            cout << "Cell, reading: data_cell = " << data_cell << endl;         
            cout << "Cell, reading: dataout.read() = " << dataout.read() << endl;         
            cout << "-----------------------------"<<endl;
         #endif                      
            
         //Inserisci delay lettura
                                    
      }
      dataout->write(data_cell);  
   }
   
}
