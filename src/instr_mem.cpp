#include <systemc.h>
#include "instr_mem.hpp"

using namespace std;

void instr_mem::leggi()
{
    static const unsigned MEM_SIZE = 64;
    sc_bv<16> line[MEM_SIZE];
    sc_bv<16> codice;
    sc_bv<3> opcode_bit;
    sc_uint<3> opcode_uint;
    line[1] ="0010010100000111"; //ADDI,rA=1,rB=2,immediate=7  : in R1 carica 7     -- R1 contiene 7
    line[2] ="0010100100000110"; //ADDI,rA=2,rB=2,immediate=6  : in R2 carica 6     -- R2 contiene 6
    line[3] ="0000110100000001"; //ADD,rA=3,rB=2, rC=1         : in R3 carica R1+R2 -- R3 contiene 13
    line[4] ="0011000110001000"; //ADDI,rA=4,rB=3, immediate=8 : in R4 carica R3+8  -- R4 contiene 21
    line[5] ="0101011100000111"; //NAND,rA=5,rB=6,rC=7         : in R5 carica R6&R7 -- R5 contiene 65535
    line[6] ="0111100000000001"; //LUI,rA=6,immediate=1        : in R6 carica 1*64  -- R6 contiene 64
    line[7] ="1011000000000001"; //SW,rA=4,address=1           : MEM[1]<=R4=21      -- MEM[1] contiene 21
    line[8] ="1001010000000001"; //LW,rA=5,address=1           : R5=MEM[1]=21       -- R5 contiene 21
    line[9] ="0010010100000111"; //ADDI,rA=1,rB=2,immediate=7 
    line[10]="0010100100000110"; //ADDI,rA=2,rB=2,immediate=6 
    line[11]="1100010100000100"; //BEQ
    line[12]="1110010000000000"; //JALR 

    while(true)
    {
      wait();
      istruzione->write(line[indirizzo->read()]);  
      cout << "   ISTRUZIONE LETTA : " << line[indirizzo->read()] << endl;
      codice=line[indirizzo->read()];
      opcode_bit=codice.range(15,13);
      opcode_uint=opcode_bit;
      cout << "   OPCODE : " << opcode_uint << endl;
      switch(opcode_uint)
      {
         case 0 : cout << "ADD" << endl; break;
      }

    }
}
