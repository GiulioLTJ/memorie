#include <systemc.h>
#include "data_memory_comportamentale.hpp"


/* io: usa DEBUG_SYSTEMC! Vedi slide 15, cap 11 */

using namespace std;

void data_memory::memory()
{
   static const unsigned MEM_DELAY = 0; //ns
   //io: userei piuttosto un sc_time, tipo SC_ZERO_TIME
   //Anche no, guarda poi come lo usa!
   //Ricorda che: SC_ZERO_TIME equivale a: sc_time(0, SC_SEC)
   
   
   static const unsigned MEM_SIZE = 1024;
   unsigned indirizzo, dato;
   unsigned short line[MEM_SIZE];  //vettore delle celle di memoria
   //Memoria inizializzata a zero
   for (unsigned i=0;i<MEM_SIZE;i++) 
   {
       line[i]=0;  
   }
   cout << "MEMORIA DATI INIZIALIZZATA A ZERO" << endl << endl;
   
   //io: non sono sicuro...
   //enable->write(0); 
   
   ready->write(1);
   
   //Memoria pronta
   
   while(true)
   {      
      //Aspetta gli ingressi
      wait();
      //wait(10,SC_NS); //io: perche?
      
      //Se enable vale 1 scrivi
      if (enable->read()==1)
      {
         ready->write(0); //io: serve una wait per attualizzarlo
         wait(SC_ZERO_TIME);
         
         line[address->read()]=datain->read();
         cout << "   MEM[" << address->read() << "] <= " << datain->read() << endl;
         /*dato memorizzato nella cella di memoria! */
         
         wait(MEM_DELAY,SC_NS);
         
         ready->write(1);
         wait(SC_ZERO_TIME);
      }
      //altrimenti leggi
      else
      {
         indirizzo=address->read();
         
         if (indirizzo<MEM_SIZE)
            dato=line[indirizzo];
         else
         {
			cout << "Indirizzo fuori dai confini della memoria!"  << endl; 
            dato=0;
		 }
            
         dataout->write(dato);
         wait(SC_ZERO_TIME);
         //io: qui metterei una wait zero, per aggiornare il canale
      }
   }
}
