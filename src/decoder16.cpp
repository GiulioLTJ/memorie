#include <systemc.h>
#include "decoder16.hpp"


using namespace std;

void Decoder16 :: decoder()
{   
   unsigned int_addr = address->read();	//type conversion
	
   for (unsigned i=0; i<MEM_SIZE; i++)
   {
      if (i==int_addr)
         addr_enable[i]->write(1);
      	
      else
         addr_enable[i]->write(0);
   }
   
   return;
}

